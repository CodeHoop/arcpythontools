from selenium import webdriver
import getpass
from selenium.webdriver.common.action_chains import ActionChains
from EmailTest import Emails

class LogInSelenium:

    # This __init__ sets the default username to Aiden Hooper, the default
    # webpage to the test OpenInvoice login page, and the user name variables.
    def __init__(self):
        self.firstName = "Aiden"
        self.lastName = "Hooper"
        self.adminName = OpenInvoiceSelenium.create_oi_login_name(self.firstName, self.lastName) 
        self.adminPassword = getpass.getpass("please enter password: ")
        self.oiurl = "https://docptest.openinvoice.com/docp/public/LoginContainer.jsp"
    
    # def password_admin_set is a method used to set the admin user's password.
    def admin_password_set(self):
        self.amdinPassword = getpass.getpass("please enter password: ")

    # def create_oi_login takes the user's first and last name and generates
    # their Open Invoice login using the standard OI naming structure.
    def create_oi_login_name(firstName, lastName):
        return "arc_" + firstName[0] + lastName
    
    # def open_firefox creates an instance of the selenium web driver that 
    # is used to navigate to the Open Invoice site.
    def open_firefox(self):
        # create a new Firefox session
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(30)
        self.driver.maximize_window()
    
        # navigate to the OpenInvoice home page
        self.driver.get(self.oiurl)

    # def enter_username is a method that enters the username and password into
    # the Open Invoice site and then logs in.
    def enter_username(self):
        # Creates a user_name object with selenium
        user_name = self.driver.find_element_by_xpath(".//*[@id='j_username']")
        user_name.clear()
        user_name.send_keys(self.adminName)
        user_password_send = self.driver.find_element_by_xpath(".//*[@id='j_password']")
        user_password_send.send_keys(self.adminPassword)
        user_password_send.submit()

    def already_signed_in():
        # you are already signed in
        signed_in = self.driver.find_element_by_xpath(".//*[@id='continueBtn']/span/button")
        signed_in.click()


    def hover_over_members():
        # hover over members
        members = driver.find_element_by_xpath(".//*[@id='mastheadNavBar']/span[5]")
        hover = ActionChains(driver).move_to_element(members)
        hover.perform()

    def unlock_users():
        # select "Unlocked Users"
        unlock_users = driver.find_element_by_xpath(
                ".//*[@id='Membership_ttd_Membership_UnlockUsers']")
        unlock_users.click()
