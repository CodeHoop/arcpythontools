from selenium import webdriver
import getpass
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import Select

# enter user password
admin_password = getpass.getpass("Please enter password: ")


# create a new Firefox session
driver = webdriver.Firefox()
driver.implicitly_wait(30)
driver.maximize_window()

# navigate to the OpenInvoice home page
driver.get("http://jpqbyteapp01:7010/AccessMgmtWeb/dbLogin.do?restart=true")

# enter username
admin_ID = driver.find_element_by_xpath(".//*[@id='Bdy']/form/table/tbody/tr[4]/td/table/tbody/tr[1]/td[2]/input")
admin_ID.clear()
admin_ID.send_keys("ahooper")
admin_password_send = driver.find_element_by_xpath(".//*[@id='Bdy']/form/table/tbody/tr[4]/td/table/tbody/tr[2]/td[2]/input")
admin_password_send.send_keys(admin_password)
environment = Select(driver.find_element_by_xpath(".//*[@id='Bdy']/form/table/tbody/tr[4]/td/table/tbody/tr[3]/td[2]/select"))
environment.select_by_visible_text("Optix - Test")
submit_button = driver.find_element_by_xpath(".//*[@id='Bdy']/form/table/tbody/tr[5]/td/input[1]")
submit_button.click()

