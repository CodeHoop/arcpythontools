# This program is used to offboard OpenInvoice users when they leave ARC.
# The program logs into OpenInvoice, searches for the user, deactivates their
# account, and removes their license.

